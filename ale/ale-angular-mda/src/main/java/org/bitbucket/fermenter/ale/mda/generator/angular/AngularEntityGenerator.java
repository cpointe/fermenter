package org.bitbucket.fermenter.ale.mda.generator.angular;

public class AngularEntityGenerator extends AbstractAngularEntityGenerator {

    @Override
    protected boolean generatePersistentEntitiesOnly() {
        return false;
    }
}
