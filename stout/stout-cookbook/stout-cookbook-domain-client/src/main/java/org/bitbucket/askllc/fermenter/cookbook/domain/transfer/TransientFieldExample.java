package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the TransientFieldExample entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.TransientFieldExampleBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class TransientFieldExample extends TransientFieldExampleBase {
}