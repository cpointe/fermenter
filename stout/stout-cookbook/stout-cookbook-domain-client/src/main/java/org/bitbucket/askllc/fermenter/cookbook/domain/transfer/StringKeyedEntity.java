package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the StringKeyedEntity entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.StringKeyedEntityBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class StringKeyedEntity extends StringKeyedEntityBase {
}