package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the DefaultValueExample entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.DefaultValueExampleBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class DefaultValueExample extends DefaultValueExampleBase {
}