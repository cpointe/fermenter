package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the TransientSubEntityExample entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.TransientSubEntityExampleBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class TransientSubEntityExample extends TransientSubEntityExampleBase {
}