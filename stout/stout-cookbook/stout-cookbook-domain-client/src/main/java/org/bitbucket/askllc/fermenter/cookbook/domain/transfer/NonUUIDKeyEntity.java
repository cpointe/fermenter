package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the NonUUIDKeyEntity entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.NonUUIDKeyEntityBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class NonUUIDKeyEntity extends NonUUIDKeyEntityBase {
}