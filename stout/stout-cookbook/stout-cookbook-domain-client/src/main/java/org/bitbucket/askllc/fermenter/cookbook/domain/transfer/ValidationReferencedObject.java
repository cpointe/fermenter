package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the ValidationReferencedObject entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.ValidationReferencedObjectBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class ValidationReferencedObject extends ValidationReferencedObjectBase{
	
}