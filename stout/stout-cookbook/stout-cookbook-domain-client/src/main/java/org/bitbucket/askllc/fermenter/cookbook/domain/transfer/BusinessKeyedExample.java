package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the BusinessKeyedExample entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.BusinessKeyedExampleBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class BusinessKeyedExample extends BusinessKeyedExampleBase {
}