package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the ServiceEntityExample entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.ServiceEntityExampleBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class ServiceEntityExample extends ServiceEntityExampleBase {
}