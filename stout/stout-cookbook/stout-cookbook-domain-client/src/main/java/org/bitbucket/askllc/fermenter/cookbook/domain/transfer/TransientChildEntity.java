package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the TransientChildEntity entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.TransientChildEntityBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class TransientChildEntity extends TransientChildEntityBase {
}