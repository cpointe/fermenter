package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the ForeignKeyColumnCustomName entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.ForeignKeyColumnCustomNameBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class ForeignKeyColumnCustomName extends ForeignKeyColumnCustomNameBase {
}