package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the ForeignKeyWithoutColumnDefinition entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.ForeignKeyWithoutColumnDefinitionBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class ForeignKeyWithoutColumnDefinition extends ForeignKeyWithoutColumnDefinitionBase {
}