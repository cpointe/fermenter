package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the TableNameMismatch entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.TableNameMismatchBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class TableNameMismatch extends TableNameMismatchBase {
}