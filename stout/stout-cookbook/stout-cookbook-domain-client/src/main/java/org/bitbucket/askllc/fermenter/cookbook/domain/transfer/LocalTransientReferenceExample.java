package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the LocalTransientReferenceExample entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.LocalTransientReferenceExampleBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class LocalTransientReferenceExample extends LocalTransientReferenceExampleBase {
}