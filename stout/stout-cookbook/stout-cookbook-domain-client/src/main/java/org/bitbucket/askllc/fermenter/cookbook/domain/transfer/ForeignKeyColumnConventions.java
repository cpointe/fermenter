package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the ForeignKeyColumnConventions entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.ForeignKeyColumnConventionsBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class ForeignKeyColumnConventions extends ForeignKeyColumnConventionsBase {
}