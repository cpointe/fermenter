package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the GenerationTestEntity entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.GenerationTestEntityBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class GenerationTestEntity extends GenerationTestEntityBase {
}