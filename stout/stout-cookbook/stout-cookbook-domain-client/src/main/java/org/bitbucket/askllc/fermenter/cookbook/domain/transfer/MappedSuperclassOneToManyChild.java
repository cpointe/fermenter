package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the MappedSuperclassOneToManyChild entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.MappedSuperclassOneToManyChildBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class MappedSuperclassOneToManyChild extends MappedSuperclassOneToManyChildBase{
	
}