package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the CachedEntityExample entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.CachedEntityExampleBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class CachedEntityExample extends CachedEntityExampleBase {
}