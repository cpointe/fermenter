package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the ForeignKeyWithColumnDefinition entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.ForeignKeyWithColumnDefinitionBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class ForeignKeyWithColumnDefinition extends ForeignKeyWithColumnDefinitionBase {
}