package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the ValidationTransientReferencedObject entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.ValidationTransientReferencedObjectBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class ValidationTransientReferencedObject extends ValidationTransientReferencedObjectBase {
}