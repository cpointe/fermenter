package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the MappedSuperclassParent entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.MappedSuperclassParentBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class MappedSuperclassParent extends MappedSuperclassParentBase{
	
}