package org.bitbucket.askllc.fermenter.cookbook.domain.transfer;


/**
 * Transfer object for the BeerExampleEntity entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.domain.transfer.BeerExampleEntityBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class BeerExampleEntity extends BeerExampleEntityBase {
}