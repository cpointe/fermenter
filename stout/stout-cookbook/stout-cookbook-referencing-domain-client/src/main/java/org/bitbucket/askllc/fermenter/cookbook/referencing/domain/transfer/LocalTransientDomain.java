package org.bitbucket.askllc.fermenter.cookbook.referencing.domain.transfer;


/**
 * Transfer object for the LocalTransientDomain entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.referencing.domain.transfer.LocalTransientDomainBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class LocalTransientDomain extends LocalTransientDomainBase {
}