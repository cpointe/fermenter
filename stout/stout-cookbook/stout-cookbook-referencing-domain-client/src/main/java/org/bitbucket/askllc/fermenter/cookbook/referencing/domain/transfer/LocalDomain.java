package org.bitbucket.askllc.fermenter.cookbook.referencing.domain.transfer;


/**
 * Transfer object for the LocalDomain entity.
 * @see org.bitbucket.askllc.fermenter.cookbook.referencing.domain.transfer.LocalDomainBase
 *
 * GENERATED STUB CODE - PLEASE *DO* MODIFY
 */
public class LocalDomain extends LocalDomainBase {
}