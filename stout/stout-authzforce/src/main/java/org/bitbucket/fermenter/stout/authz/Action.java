package org.bitbucket.fermenter.stout.authz;

/**
 * Represents the actions used by Fermenter for authorization.
 */
public enum Action {

    SAVE, RETRIEVE, DELETE, EXECUTE;

}
